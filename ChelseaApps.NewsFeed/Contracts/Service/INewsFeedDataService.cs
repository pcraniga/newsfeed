﻿using System.Threading.Tasks;
using ChelseaApps.NewsFeed.Models;

namespace ChelseaApps.NewsFeed.Contracts.Service
{
    public interface INewsFeedDataService
    {
        Task<RssObject> GetFeed();
    }
}
