﻿using System.Threading.Tasks;
using ChelseaApps.NewsFeed.Models;

namespace ChelseaApps.NewsFeed.Contracts.Repository
{
    public interface INewsFeedRepository
    {
        Task<RssObject> GetFeed();
    }
}
