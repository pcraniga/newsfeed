﻿using System.Threading.Tasks;
using ChelseaApps.NewsFeed.Contracts.Repository;
using ChelseaApps.NewsFeed.Contracts.Service;
using ChelseaApps.NewsFeed.Models;

namespace ChelseaApps.NewsFeed.Services
{
    public class NewsFeedDataService : INewsFeedDataService
    {
        private readonly INewsFeedRepository _newsFeedRepository;

        public NewsFeedDataService(INewsFeedRepository newsFeedRepository)
        {
            _newsFeedRepository = newsFeedRepository;
        }

        public async Task<RssObject> GetFeed()
        {
            return await _newsFeedRepository.GetFeed();
        }
    }
}
