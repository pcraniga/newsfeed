﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using ChelseaApps.NewsFeed.Contracts.Service;
using ChelseaApps.NewsFeed.Contracts.ViewModel;
using ChelseaApps.NewsFeed.Extensions;
using ChelseaApps.NewsFeed.Models;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace ChelseaApps.NewsFeed.ViewModels
{
    public class NewsArticlesViewModel : BaseViewModel, INewsArticlesViewModel
    {
        private readonly INewsFeedDataService _newsFeedDataService;
        private RssObject _rssObject;
        private string _title;
        private ObservableCollection<Item> _newsArticles;

        public NewsArticlesViewModel(IMvxMessenger messenger, INewsFeedDataService newsDataService) : base(messenger)
        {
            _newsFeedDataService = newsDataService;

            InitializeMessenger();
        }

        public MvxCommand ReloadDataCommand
        {
            get
            {
                return new MvxCommand(async () =>
                {
                    RssFeed = await _newsFeedDataService.GetFeed();
                    Title = RssFeed.feed.title;
                });
            }
        }

        public RssObject RssFeed
        {
            get
            {
                return _rssObject;
            }
            set
            {
                _rssObject = value;
                RaisePropertyChanged(() => RssFeed);
            }
        }

        public ObservableCollection<Item> NewsArticles
        {
            get { return _newsArticles; }
            set
            {
                _newsArticles = value;
                RaisePropertyChanged(() => NewsArticles);
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        public MvxCommand<Item> ShowNewArticleCommand
        {
            get
            {
                return new MvxCommand<Item>(selectedArticle =>
                {
                    ShowViewModel<NewsArticleViewModel>
                    (new { link = selectedArticle.link, title = selectedArticle.title });
                });
            }
        }

        public override async void Start()
        {
            base.Start();
            await ReloadDataAsync();
        }

        protected override async Task InitializeAsync()
        {
            RssFeed = await _newsFeedDataService.GetFeed();

            if (RssFeed != null && RssFeed.feed != null)
            {
                if (RssFeed.items != null)
                {
                    NewsArticles = RssFeed.items.ToObservableCollection();
                }
                Title = RssFeed.feed.title;
            }
        }

        private void InitializeMessenger()
        {

        }

    }
}