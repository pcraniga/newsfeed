﻿using ChelseaApps.NewsFeed.Contracts.ViewModel;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.Messenger;

namespace ChelseaApps.NewsFeed.ViewModels
{
    public class NewsArticleViewModel : BaseViewModel, INewsArticleViewModel
    {
        private string _link;
        private string _title;

        public NewsArticleViewModel(IMvxMessenger messenger) : base(messenger)
        {
        }

        public string Link
        {
            get { return _link; }
            set
            {
                _link = value;
                RaisePropertyChanged(() => Link);
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged(() => Title);
            }
        }

        public void Init(string link, string title)
        {
            Link = link;
            Title = title;
        }

        public MvxCommand CloseCommand
        {
            get
            {
                return new MvxCommand(() => Close(this));
            }
        }
    }
}
