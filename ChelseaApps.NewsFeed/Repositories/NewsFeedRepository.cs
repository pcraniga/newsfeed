﻿using System.Threading.Tasks;
using ChelseaApps.NewsFeed.Contracts.Repository;
using ChelseaApps.NewsFeed.Models;

namespace ChelseaApps.NewsFeed.Repositories
{
    public class NewsFeedRepository : BaseRepository, INewsFeedRepository
    {
        private const string RSS_LINK = "http://feeds.bbci.co.uk/news/technology/rss.xml";
        private const string RSS_TO_JSON = "https://api.rss2json.com/v1/api.json?rss_url=";


        public Task<RssObject> GetFeed()
        {
            return this.GetAsync<RssObject>(RSS_TO_JSON + RSS_LINK);
        }
    }
}
