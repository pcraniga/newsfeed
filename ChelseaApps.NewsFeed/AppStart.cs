﻿using ChelseaApps.NewsFeed.ViewModels;
using MvvmCross.Core.ViewModels;

namespace ChelseaApps.NewsFeed
{
    public class AppStart : MvxNavigatingObject, IMvxAppStart
    {
        public async void Start(object hint = null)
        {
            ShowViewModel<NewsArticlesViewModel>();
        }
    }
}
