﻿using System;

namespace ChelseaApps.NewsFeed.Models
{
    public class NewsArticle
    {
        public string Url { get; set; }
    }
}
