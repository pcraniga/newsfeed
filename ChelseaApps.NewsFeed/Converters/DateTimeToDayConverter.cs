﻿using System;
using MvvmCross.Platform.Converters;

namespace ChelseaApps.NewsFeed.Converters
{
    public class DateTimeToDayConverter : MvxValueConverter<string, string>
    {
        protected override string Convert(string value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            var date = DateTime.Parse(value);
            var returnValue = date.ToString("dd MMM, yyyy");
            return returnValue;
        }
    }
}
