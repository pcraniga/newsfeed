using MvvmCross.Platform.Plugins;

namespace ChelseaApps.NewsFeed.iOS.Bootstrap
{
    public class MessengerPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Messenger.PluginLoader>
    {
    }
}