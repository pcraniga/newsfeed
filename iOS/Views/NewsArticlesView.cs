﻿using System;
using ChelseaApps.NewsFeed.iOS.TableViewSources;
using ChelseaApps.NewsFeed.ViewModels;
using MvvmCross.Binding.BindingContext;

namespace ChelseaApps.NewsFeed.iOS.Views
{
    public partial class NewsArticlesView : BaseView
    {
        private NewsArticlesTableViewSource _newsArticlesTableViewSource;

        protected NewsArticlesViewModel NewsArticlesViewModel => ViewModel as NewsArticlesViewModel;

        public NewsArticlesView(IntPtr handle) : base(handle)
        {
        }

        protected override void CreateBindings()
        {
            base.CreateBindings();

            var set = this.CreateBindingSet<NewsArticlesView, NewsArticlesViewModel>();

            set.Bind().For(v => v.Title).To(vm => vm.Title);
            set.Bind(_newsArticlesTableViewSource).To(vm => vm.NewsArticles);
            set.Bind(_newsArticlesTableViewSource)
                .For(source => source.SelectionChangedCommand)
                .To(vm => vm.ShowNewArticleCommand);

            set.Apply();
        }

        public override void ViewDidLoad()
        {
            _newsArticlesTableViewSource = new NewsArticlesTableViewSource(newsArticlesTableView);

            base.ViewDidLoad();

            newsArticlesTableView.Source = _newsArticlesTableViewSource;
            newsArticlesTableView.ReloadData();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            newsArticlesTableView.RowHeight = 65f;

            NewsArticlesViewModel.ReloadDataCommand.Execute();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }
    }
}

