﻿using System;
using ChelseaApps.NewsFeed.ViewModels;
using Foundation;
using MvvmCross.Binding.BindingContext;

namespace ChelseaApps.NewsFeed.iOS.Views
{
    public partial class NewsArticleView : BaseView
    {

        protected NewsArticleViewModel NewsArticleViewModel => ViewModel as NewsArticleViewModel;

        public NewsArticleView(IntPtr handle) : base(handle)
        {
        }

        protected override void CreateBindings()
        {
            base.CreateBindings();

            var set = this.CreateBindingSet<NewsArticleView, NewsArticleViewModel>();
            set.Bind().For(v => v.Title).To(vm => vm.Title);

            set.Apply();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            string link = ((NewsArticleViewModel)ViewModel).Link;
            webView.LoadRequest(new NSUrlRequest(new NSUrl(link)));
            webView.ScalesPageToFit = true;
        }
    }
}

