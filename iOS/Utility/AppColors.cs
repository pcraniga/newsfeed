﻿using UIKit;

namespace ChelseaApps.NewsFeed.iOS.Utility
{
    public static class AppColors
    {
        public static UIColor AccentColor = new UIColor(59 / 255f, 202 / 255f, 219 / 255f, 1);

        public static UIColor DarkColor = new UIColor(94 / 255f, 149 / 255f, 154 / 255f, 1);

        public static UIColor DarkTextColor = new UIColor(55 / 255f, 59 / 255f, 73 / 255f, 1);

        public static UIColor BorderColor = new UIColor(225 / 255f, 225 / 255f, 225 / 255f, 1);
    }
}