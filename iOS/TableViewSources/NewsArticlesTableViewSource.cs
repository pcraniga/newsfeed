﻿using System;
using Foundation;
using MvvmCross.Binding.ExtensionMethods;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace ChelseaApps.NewsFeed.iOS.TableViewSources
{
    public class NewsArticlesTableViewSource : MvxTableViewSource
    {
        public NewsArticlesTableViewSource(UITableView tableView) : base(tableView)
        {
        }

        public NewsArticlesTableViewSource(IntPtr handle) : base(handle)
        {
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return ItemsSource.Count();
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            var cell = (NewsArticlesCell)tableView.DequeueReusableCell(NewsArticlesCell.Identifier);
            return cell;
        }
    }
}
