using System;
using ChelseaApps.NewsFeed.Converters;
using ChelseaApps.NewsFeed.Models;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;

namespace ChelseaApps.NewsFeed.iOS
{
    public partial class NewsArticlesCell : MvxTableViewCell
    {
        public NewsArticlesCell(IntPtr handle) : base(handle)
        {
        }

        internal static NSString Identifier = new NSString("NewsArticlesCell");

        private void CreateBindings()
        {
            var set = this.CreateBindingSet<NewsArticlesCell, Item>();

            set.Bind(title)
                .To(vm => vm.title);

            set.Bind(publishDate)
                .To(vm => vm.pubDate)
                .WithConversion(new DateTimeToDayConverter());

            set.Apply();
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            CreateBindings();
        }

    }
}