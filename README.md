# Chelsea Apps Factory - News Feed #

This News Feed application is written in C# using Xamarin and targets the iOS platform. It retrieves a list of articles from the BBC RSS feed. The user is able to select an article from the list to read it in the embedded web view.

### Basic functionality ###

* Load news articles from a public news feed
* Display a scrollable list of news articles
* Provide the option to filter news articles by category
* Show a single news article on screen, using an HTML display

### Technology Stack ###

* C#
* Xamarin (Classic)
* MvvmCross

### Areas For Improvement ###
* Code comments
* Logging and exception handling
* Unit and automated tests
* UI enhancements such as adding thumbnails to the list-view
* Responsive UI to handle various form factors and orientations
* Add Android support
* Caching to support off-line reading

### Sample Screen Captures ###
![alt text](https://bytebucket.org/pcraniga/newsfeed/raw/abe9ca5e471bc3f4117a0ac4ece93cd652802d9f/screenshots/list.png)
![alt text](https://bytebucket.org/pcraniga/newsfeed/raw/abe9ca5e471bc3f4117a0ac4ece93cd652802d9f/screenshots/webview.png)